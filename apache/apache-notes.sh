#! /bin/bash

# https://mimiz.github.io/2017/05/18/Configure-docker-httpd-image.html

# check ports
sudo netstat -tulpn | grep LISTEN

#
sudo netstat -ap | grep :<port_number>

# C:\Windows\System32\Drivers\etc\hosts to apply dns to localhost


docker network create \
--driver bridge \
--subnet 172.100.100.0/24 \
mmsbridge


# pull docker image
docker pull httpd

# create a volume for the configuration data
docker volume create apacheconf

# view default files
docker run \
--rm \
-it \
httpd:latest bash

# run temp container to copy httpd.conf in or out of volume
docker run \
--rm \
--mount type=bind,source=/home/coder/is4-quickstart/apache/conf/,target=/host-local \
-v apacheconf:/usr/local/apache2/conf \
ubuntu:latest bash \
-c "cp /host-local/httpd.identity.conf /usr/local/apache2/conf/httpd.conf"
-c "cp /usr/local/apache2/conf/httpd.conf /host-local/httpd.identity.conf"
# --mount type=bind,source="$(pwd)"/conf,target=/host-local \

# edit files in volume
docker run \
-it --rm \
-v apacheconf:/usr/local/apache2/conf \
vim_editor:latest bash \
-c "vim /usr/local/apache2/conf/httpd.conf"

# create docker container
docker create \
-p 80:80 \
--name mcsapache \
-v apacheconf:/usr/local/apache2/conf \
-v apachesites:/mms \
httpd:latest
-p 443:443 




###############################################################################
# install self signed keys
###############################################################################

# generate keys
openssl req -x509 \
-nodes -days 365 \
-newkey rsa:2048 \
-keyout mms-selfsigned.key \
-out mms-selfsigned.crt

# copy keys to volume
docker run \
--rm \
--mount type=bind,source=/home/coder/is4-quickstart/apache/certs,target=/host-local \
-v apachecerts:/mms-certs \
ubuntu:latest bash \
-c "cp -R /host-local/* /mms-certs/"

# view keys
docker run \
-it --rm \
-v apachecerts:/mms-certs \
vim_editor:latest bash

# edit config
docker run \
-it --rm \
-v apacheconf:/usr/local/apache2/conf \
vim_editor:latest bash \
-c "vim /usr/local/apache2/conf/httpd.conf"

docker run \
-it --rm \
-v dotnetcerts:/certs \
vim_editor:latest bash