using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.HttpOverrides;
using System.Net;

namespace Api
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<ForwardedHeadersOptions>(options =>
            {
                options.ForwardedHeaders = ForwardedHeaders.XForwardedHost | ForwardedHeaders.XForwardedProto | ForwardedHeaders.XForwardedFor;
                // options.KnownNetworks.Clear();
                // options.KnownProxies.Clear();
                options.KnownProxies.Add(IPAddress.Parse("172.100.100.10"));
            });

            services.AddControllers();

            services.AddAuthentication("Bearer")
            .AddJwtBearer("Bearer", options =>
            {
                // options.Authority = "https://identity.workermonkeyrobot.com";
                // options.Audience = "https://identity.workermonkeyrobot.com/resources";
                
                //HACK: security risk. must learn to resolve https://identity.workermonkeyrobot.com
                options.Authority = "http://172.100.100.12:5000";
                options.RequireHttpsMetadata = false;


                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateAudience = false,
                    //HACK: security risk. must learn to resolve https://identity.workermonkeyrobot.com
                    ValidateIssuer = false
                };
            });

            services.AddAuthorization(options =>
            {
                options.AddPolicy("ApiScope", policy =>
                {
                    policy.RequireAuthenticatedUser();
                    policy.RequireClaim("scope", "api1");
                });
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env,
                      ILogger<Startup> _logger)
        {
            app.UseForwardedHeaders();

            app.Use(async (context, next) =>
            {
                // Request method, scheme, and path
                _logger.LogDebug("Request Method: {Method}", context.Request.Method);
                _logger.LogDebug("Request Scheme: {Scheme}", context.Request.Scheme);
                _logger.LogDebug("Request Path: {Path}", context.Request.Path);

                // Headers
                foreach (var header in context.Request.Headers)
                {
                    _logger.LogDebug("Header: {Key}: {Value}", header.Key, header.Value);
                }

                // Connection: RemoteIp
                _logger.LogDebug("Request RemoteIp: {RemoteIpAddress}", 
                    context.Connection.RemoteIpAddress);

                await next();

            });
            
            // if (env.IsDevelopment())
            // {
            //     app.UseDeveloperExceptionPage();
            // }

            // app.UseCors("AllowAnyOrigin");
            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers()
                    .RequireAuthorization("ApiScope");
            });

        }
    }
}
