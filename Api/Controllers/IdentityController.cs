using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace Api.Controllers
{
    public class jsonmms
    {
        public string msg;
    }

    [Route("identity")]
    [Authorize]
    public class IdentityController : ControllerBase
    {
        // public string Get()
        public IActionResult Get()
        {
            return new JsonResult(from c in User.Claims select new { c.Type, c.Value });
        }
    }
}